GOOS ?= $(shell go env GOOS)
GOARCH ?= $(shell go env GOARCH)
GOHOST ?= GOOS=$(GOOS) GOARCH=$(GOARCH)


build-api:
	env $(GOHOST) go build -o . --mod=vendor ./cmd/api
run-api:
	make build-api
	./api
test:
	go test ./... -v