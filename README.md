# Todo Api

### Description
This is an example project for a todo REST api in Go with a mongodb document store.

### REST API

URL: localhost:9000

| Method | Endpoint | Description | Example Body |
|--------| -------- | ----------- | ---- |
| GET | /todos | Fetch all todos |  ------|
| POST | /todos | Create a todo | ```{ "name": "Todo", "description": "First Todo here", "tasks": [{ "taskid": 1, "taskname": "First Task", "taskdescription":"Test" }]} ```|
| PUT | /todos/{todoID} | Update a todo by todoID |  ``` { "name": "Todo", "description": "First Todo here", "tasks": [{ "taskid": 1, "taskname": "First Task", "taskdescription":"Test" }]} ``` |
| GET | /todos/{todoID} | Fetch a todo by todoID |  ------ |
| DELETE | /todos/{todoID} | Delete a todo by todoID |  ------ |

### Initial Development Setup
#### Get Packages
```bash
go mod vendor
```

#### Start Docker
```bash
docker-compose up
```

#### Build & Run Go Executable
```bash
go build -o . --mod=vendor ./cmd/api
./api
```

OR 

```bash
make run-api
```

#### Test Code
```bash
go test ./...
```

OR

```bash
make test
```