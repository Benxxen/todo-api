package main

import (
	"context"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo"

	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
)

func todoService(ctx context.Context, db *mongo.Client, wg *sync.WaitGroup) {
	router := createRouter(ctx, db)
	server := http.Server{
		Addr:         ":9000",
		Handler:      router,
		ReadTimeout:  time.Duration(15) * time.Second,
		WriteTimeout: time.Duration(15) * time.Second,
		IdleTimeout:  time.Duration(60) * time.Second,
	}

	var serverShutdown sync.WaitGroup
	serverShutdown.Add(1)
	go func() {
		defer log.Debug().Msg("server has stopped")
		defer serverShutdown.Done()
		err := server.ListenAndServe()
		log.Info().Msg("server is listening")
		if err != nil {
			log.Error().Err(err).Msg("server stopped working")
		}
	}()

	<-ctx.Done()
	log.Debug().Msg("stopping web listener gracefully")
	gracefulCtx, cf := context.WithTimeout(context.Background(), 15*time.Second)
	defer cf()
	err := server.Shutdown(gracefulCtx)
	if err != nil {
		log.Error().Err(err).Msg("couldn´t stop server gracefully")
	}
	serverShutdown.Wait()
	wg.Done()
}

func createRouter(ctx context.Context, db *mongo.Client) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/todos", readTodosView(ctx, db)).Methods(http.MethodGet)
	r.HandleFunc("/todos", createTodoView(ctx, db)).Methods(http.MethodPost)
	r.HandleFunc("/todos/{todoId}", updateTodoView(ctx, db)).Methods(http.MethodPut)
	r.HandleFunc("/todos/{todoId}", readTodoView(ctx, db)).Methods(http.MethodGet)
	r.HandleFunc("/todos/{todoId}", deleteTodoView(ctx, db)).Methods(http.MethodDelete)
	r.NotFoundHandler = notFoundHandler()

	return r
}

func notFoundHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		_, err := w.Write([]byte("\"Not found\""))
		if err != nil {
			log.Err(err).Msg("couldn´t write response")
		}
	})
}
