package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DatabaseHandler struct {
	Connection *mongo.Client
}

type Todo struct {
	ID          primitive.ObjectID `bson:"_id,omitempty"`
	Name        string             `validate:"required"`
	Description string
	Tasks       []Task `validate:"dive"`
}

type Task struct {
	TaskID          int    `validate:"required"`
	TaskName        string `validate:"required"`
	TaskDescription string `bson:"taskDescription"`
}

var database DatabaseHandler

func createDatabaseConnection(ctx context.Context) (DatabaseHandler, error) {
	databaseURI := "mongodb://localhost:27017"
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(databaseURI))
	if err != nil {
		return DatabaseHandler{}, err
	}

	return DatabaseHandler{Connection: client}, nil
}

func readTodosView(ctx context.Context, db *mongo.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		todos, err := readTodos(ctx, db)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			log.Error().Err(err).Msg("couldn´t read todos")
			return
		}

		jsonResponse, err := json.Marshal(todos)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			log.Err(err).Msg("couldn´t marshal json response")
			return
		}

		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(jsonResponse)

	}
}

func readTodos(ctx context.Context, db *mongo.Client) ([]Todo, error) {
	var foundTodos []Todo

	collection := db.Database("todoService").Collection("todos")

	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	for cursor.Next(ctx) {
		var nextTodo Todo
		err = cursor.Decode(&nextTodo)
		if err != nil {
			return nil, err
		}

		foundTodos = append(foundTodos, nextTodo)
	}

	return foundTodos, nil
}

func readTodoView(ctx context.Context, db *mongo.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		todoId, exists := mux.Vars(r)["todoId"]
		if exists != true {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte("\"given id is invalid\""))
			return
		}

		todo, err := readTodo(ctx, db, todoId)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			log.Error().Err(err).Msg("couldn´t read todos")
			return
		}

		jsonResponse, err := json.Marshal(todo)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			log.Err(err).Msg("couldn´t marshal json response")
			return
		}

		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(jsonResponse)
	}
}

func readTodo(ctx context.Context, db *mongo.Client, todoId string) (Todo, error) {
	var foundTodo Todo

	collection := db.Database("todoService").Collection("todos")

	objectId, err := primitive.ObjectIDFromHex(todoId)
	if err != nil {
		return Todo{}, err
	}

	result := collection.FindOne(ctx, bson.M{"_id": objectId})
	if result.Err() != nil {
		return Todo{}, result.Err()
	}

	err = result.Decode(&foundTodo)
	if err != nil {
		return Todo{}, fmt.Errorf("couldn´t decode result: %v", result)
	}

	return foundTodo, nil
}

func createTodoView(ctx context.Context, db *mongo.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var givenTodo Todo
		err := json.NewDecoder(r.Body).Decode(&givenTodo)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte("\"given json object is invalid\""))
			log.Error().Err(err).Msg("couldn´t decode json object")
			return
		}

		validate := validator.New()
		err = validate.Struct(givenTodo)
		if err != nil {
			var missingFields []string
			for _, err := range err.(validator.ValidationErrors) {
				missingFields = append(missingFields, err.Field())
			}
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(fmt.Sprintf("required fields are missing: %v", missingFields)))
			log.Error().Err(err).Msg("required fields are missing")
			return
		}

		createdTodo, err := createTodo(ctx, db, givenTodo)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			log.Error().Err(err).Msg("couldn´t create todo")
			return
		}

		jsonResponse, err := json.Marshal(createdTodo)
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(jsonResponse)
	}
}

func createTodo(ctx context.Context, db *mongo.Client, todo Todo) (Todo, error) {
	collection := db.Database("todoService").Collection("todos")

	result, err := collection.InsertOne(ctx, todo)
	if err != nil {
		return Todo{}, err
	}

	todo.ID, err = primitive.ObjectIDFromHex(result.InsertedID.(primitive.ObjectID).Hex())
	if err != nil {
		return Todo{}, err
	}

	return todo, nil
}

func updateTodoView(ctx context.Context, db *mongo.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var givenTodo Todo
		id := mux.Vars(r)["todoId"]

		err := json.NewDecoder(r.Body).Decode(&givenTodo)
		if err != nil {
			log.Error().Err(err).Msg("couldn´t decode request body")
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte("\"given json object is invalid\""))
			return
		}

		validate := validator.New()
		err = validate.Struct(givenTodo)
		if err != nil {
			var missingFields []string
			for _, err := range err.(validator.ValidationErrors) {
				missingFields = append(missingFields, err.Field())
			}
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte(fmt.Sprintf("required fields are missing: %v", missingFields)))
			log.Error().Err(err).Msg("required fields are missing")
			return
		}

		_, err = updateTodo(ctx, db, givenTodo, id)
		if err != nil {
			log.Error().Err(err).Msg("couldn´t update todo")
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			return
		}

		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("\"Ok\""))
	}
}
func updateTodo(ctx context.Context, db *mongo.Client, todo Todo, todoId string) (int64, error) {
	collection := db.Database("todoService").Collection("todos")

	objectId, err := primitive.ObjectIDFromHex(todoId)
	if err != nil {
		return 0, err
	}

	update := bson.D{{"$set", todo}}

	result, err := collection.UpdateOne(ctx, bson.D{{"_id", objectId}}, update)
	if err != nil {
		return 0, err
	}

	return result.ModifiedCount, nil
}

func deleteTodoView(ctx context.Context, db *mongo.Client) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		todoId, exists := mux.Vars(r)["todoId"]
		if exists != true {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write([]byte("\"given id is invalid\""))
			return
		}

		_, err := deleteTodo(ctx, db, todoId)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write([]byte("\"something bad happened\""))
			log.Error().Err(err).Msg("couldn´t read todos")
			return
		}

		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("\"Ok\""))
	}
}

func deleteTodo(ctx context.Context, db *mongo.Client, todoId string) (int64, error) {
	collection := db.Database("todoService").Collection("todos")

	objectId, err := primitive.ObjectIDFromHex(todoId)
	if err != nil {
		return 0, err
	}

	result, err := collection.DeleteOne(ctx, bson.D{{"_id", objectId}})
	if err != nil {
		return 0, err
	}

	return result.DeletedCount, nil
}
