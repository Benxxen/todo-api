package main

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var testTodos = []Todo{{primitive.NewObjectID(), "Todo 1", "Todo 1 is here", []Task{{1, "Task 1", "Task 1 is here"}}}, {primitive.NewObjectID(), "Todo 2", "Todo 2 here", []Task{}}}

func TestCreateTodoView(t *testing.T) {
	var resultTodo Todo
	var expectedTodo Todo

	//Test Cases
	tts := []struct {
		name          string
		inp           map[string]interface{}
		exp           map[string]interface{}
		expStatusCode int
		expErr        bool
		expErrMsg     string
	}{
		{"simple-write", map[string]interface{}{"Name": "Foo", "Description": "Bar", "Tasks": []map[string]interface{}{{"TaskID": 1, "TaskName": "Task 1", "TaskDescription": "Task 1 is here"}}}, map[string]interface{}{"Name": "Foo", "Description": "Bar", "Tasks": []map[string]interface{}{{"TaskID": 1, "TaskName": "Task 1", "TaskDescription": "Task 1 is here"}}}, http.StatusOK, false, ""},
		{"required-field-name-missing", map[string]interface{}{"description": "TEST", "tasks": []map[string]interface{}{{"taskId": 2, "taskName": "Task 1", "taskDescription": "Task 1 is here"}}}, map[string]interface{}{}, http.StatusBadRequest, true, "required fields are missing: [Name]"},
		{"required-task-id-missing", map[string]interface{}{"name": "Foo", "description": "TEST", "tasks": []map[string]interface{}{{"taskName": "Task 1", "taskDescription": "Task 1 is here"}}}, map[string]interface{}{}, http.StatusBadRequest, true, "required fields are missing: [TaskID]"},
		{"required-task-name-missing", map[string]interface{}{"name": "Foo", "description": "TEST", "tasks": []map[string]interface{}{{"taskId": 2, "taskDescription": "Task 1 is here"}}}, map[string]interface{}{}, http.StatusBadRequest, true, "required fields are missing: [TaskName]"},
	}

	for _, tt := range tts {
		tf := func(t *testing.T) {
			jsonInput, _ := json.Marshal(tt.inp)
			router, respRecorder := setupTestEnv()

			req, _ := http.NewRequest("POST", "/todos", bytes.NewBuffer(jsonInput))
			router.ServeHTTP(respRecorder, req)

			expTodo, _ := json.Marshal(tt.exp)
			_ = json.NewDecoder(bytes.NewBuffer(expTodo)).Decode(&expectedTodo)

			_ = json.NewDecoder(bytes.NewBuffer(respRecorder.Body.Bytes())).Decode(&resultTodo)

			//Assertion status code
			assertStatusCode(t, tt.expStatusCode, respRecorder.Code)

			if tt.expErr != true {
				if resultTodo.ID == primitive.NilObjectID {
					t.Fatalf("\t%s: should return new primitive object id but returned: %v", failed, resultTodo.ID)
				} else {
					t.Logf("\t%s: should return new primitive object id but returned", succeed)
				}
				if resultTodo.Name != expectedTodo.Name {
					t.Fatalf("\t%s: should return same todo name: %v, but returned: %v", failed, expectedTodo.Name, resultTodo.Name)
				} else {
					t.Logf("\t%s: should return same todo name", succeed)
				}
				if resultTodo.Description != expectedTodo.Description {
					t.Fatalf("\t%s: should return same todo description: %v, but returned: %v", failed, expectedTodo.Description, resultTodo.Description)
				} else {
					t.Logf("\t%s: should return same todo description", succeed)
				}
				if reflect.DeepEqual(resultTodo.Tasks, expectedTodo.Tasks) != true {
					t.Fatalf("\t%s: should return same todo tasks: %v, but returned: %v", failed, expectedTodo.Tasks, resultTodo.Tasks)
				} else {
					t.Logf("\t%s: should return same todo tasks", succeed)
				}
			} else {
				if respRecorder.Body.String() != tt.expErrMsg {
					t.Fatalf("\t%s: response should contain error msg: %s, but got: %s", failed, tt.expErrMsg, respRecorder.Body.String())
				} else {
					t.Logf("\t%s: response should contain error msg", succeed)
				}
			}
			tearDownCollection()
		}

		t.Run(tt.name, tf)
	}
}

func TestReadTodosView(t *testing.T) {
	var resultTodos []Todo
	router, respRecorder := setupTestEnv()
	insertTestData()
	req, _ := http.NewRequest("GET", "/todos", nil)
	router.ServeHTTP(respRecorder, req)

	//Assertion status code
	assertStatusCode(t, http.StatusOK, respRecorder.Code)

	err := json.Unmarshal(respRecorder.Body.Bytes(), &resultTodos)
	if err != nil {
		t.Fatalf("\t%s: should be able to unmarshal body to todos: %v", failed, err)
	} else {
		t.Logf("\t%s: should be able to unmarshal body to todos", succeed)
	}

	for v, todo := range testTodos {
		if reflect.DeepEqual(todo, resultTodos[v]) != true {
			t.Fatalf("\t%s: should equal struct: %v, but got: %v", failed, todo, resultTodos[v])
		} else {
			t.Logf("\t%s: should equal struct", succeed)
		}
	}

	tearDownCollection()
}

func TestReadTodoView(t *testing.T) {
	var resultTodo Todo
	router, respRecorder := setupTestEnv()
	insertTestData()

	req, _ := http.NewRequest("GET", "/todos/"+testTodos[0].ID.Hex(), nil)
	router.ServeHTTP(respRecorder, req)

	//Assertion status code
	assertStatusCode(t, http.StatusOK, respRecorder.Code)

	//Convert response body to struct to make comparison easier
	err := json.Unmarshal(respRecorder.Body.Bytes(), &resultTodo)
	if err != nil {
		t.Fatalf("\t%s: should be able to unmarshal body to todo: %v", failed, err)
	} else {
		t.Logf("\t%s: should be able to unmarshal body to todo", succeed)
	}

	//Assertion if structs are equal
	if reflect.DeepEqual(resultTodo, testTodos[0]) != true {
		t.Fatalf("\t%s: should equal test todo: %v, but returned: %v ", failed, testTodos[0], resultTodo)
	} else {
		t.Logf("\t%s: should equal test todo", succeed)
	}

	//Drop collection
	tearDownCollection()
}

func TestUpdateTodoView(t *testing.T) {
	testID := testTodos[0].ID
	var resultTodo Todo

	//Test Cases
	tts := []struct {
		name          string
		inp           Todo
		exp           Todo
		expStatusCode int
		expErr        bool
		expErrMsg     string
	}{
		{"simple-write", Todo{ID: testID, Name: "Todo 5", Description: "Todo 5 is here", Tasks: []Task{{TaskID: 1, TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, Todo{ID: testID, Name: "Todo 5", Description: "Todo 5 is here", Tasks: []Task{{TaskID: 1, TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, http.StatusOK, false, ""},
		{"required-field-name-is-missing", Todo{ID: primitive.NilObjectID, Tasks: []Task{{TaskID: 1, TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, Todo{ID: primitive.NilObjectID, Tasks: []Task{{TaskID: 1, TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, http.StatusBadRequest, true, "required fields are missing: [Name]"},
		{"required-field-task-id-is-missing", Todo{Name: "Test", Tasks: []Task{{TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, Todo{Name: "Test", Tasks: []Task{{TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, http.StatusBadRequest, true, "required fields are missing: [TaskID]"},
		{"required-field-task-name-is-missing", Todo{Name: "Test", Tasks: []Task{{TaskID: 1, TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, Todo{Name: "Test", Tasks: []Task{{TaskName: "Task 1", TaskDescription: "Task 1 is here"}, {TaskID: 2, TaskName: "Task 2", TaskDescription: ""}}}, http.StatusBadRequest, true, "required fields are missing: [TaskName]"},
	}

	for _, tt := range tts {
		tf := func(t *testing.T) {
			jsonInput, _ := json.Marshal(tt.inp)
			router, respRecorder := setupTestEnv()
			insertTestData()

			req, _ := http.NewRequest("PUT", "/todos/"+tt.exp.ID.Hex(), bytes.NewBuffer(jsonInput))
			router.ServeHTTP(respRecorder, req)

			_ = json.NewDecoder(bytes.NewBuffer(respRecorder.Body.Bytes())).Decode(&resultTodo)

			//Assertion status code
			assertStatusCode(t, tt.expStatusCode, respRecorder.Code)

			//Fetch updated Document
			if tt.expErr != true {
				foundTodo, err := readTodo(context.Background(), database.Connection, tt.exp.ID.Hex())
				if err != nil {
					t.Fatalf("\t%s: should find updated todo: %v", failed, tt.exp)
				} else {
					t.Logf("\t%s: should find updated todo", succeed)
				}

				//Assertion if structs are equal
				if reflect.DeepEqual(foundTodo, tt.exp) != true {
					t.Fatalf("\t%s: should equal updated test todo: %v, but returned: %v ", failed, tt.exp, foundTodo)
				} else {
					t.Logf("\t%s: should equal updated test todo", succeed)
				}
			} else {
				if respRecorder.Body.String() != tt.expErrMsg {
					t.Fatalf("\t%s: response should contain error msg: %s, but got: %s", failed, tt.expErrMsg, respRecorder.Body.String())
				} else {
					t.Logf("\t%s: response should contain error msg", succeed)
				}
			}

			//Drop collection
			tearDownCollection()
		}
		t.Run(tt.name, tf)

	}
}

func TestDeleteTodoView(t *testing.T) {
	router, respRecorder := setupTestEnv()
	insertTestData()

	req, _ := http.NewRequest("DELETE", "/todos/"+testTodos[0].ID.Hex(), nil)
	router.ServeHTTP(respRecorder, req)

	//Assertion status code
	assertStatusCode(t, http.StatusOK, respRecorder.Code)

	//Fetch updated Document
	foundTodo, err := readTodo(context.Background(), database.Connection, testTodos[0].ID.Hex())
	if err != nil && err.Error() != "mongo: no documents in result" {
		t.Fatalf("\t%s: shouldn´t find deleted todo: %v", failed, foundTodo)
	} else {
		t.Logf("\t%s: shouldn´t find deleted todo", succeed)
	}

	//Drop collection
	tearDownCollection()
}

func assertStatusCode(t *testing.T, expectedStatusCode int, resultStatusCode int) {
	if expectedStatusCode != resultStatusCode {
		t.Fatalf("\t%s: should return response code: %d, but got %d", failed, expectedStatusCode, resultStatusCode)
	} else {
		t.Logf("\t%s: should return response code %d", succeed, expectedStatusCode)
	}

}

func setupTestEnv() (*mux.Router, *httptest.ResponseRecorder) {
	database, _ = createDatabaseConnection(context.Background())
	router := createRouter(context.Background(), database.Connection)
	rr := httptest.NewRecorder()

	return router, rr
}

func insertTestData() {
	database, _ = createDatabaseConnection(context.Background())
	collection := database.Connection.Database("todoService").Collection("todos")

	for _, todo := range testTodos {
		_, _ = collection.InsertOne(context.Background(), todo)
	}
}

func tearDownCollection() {
	_ = database.Connection.Database("todoService").Collection("todos").Drop(context.Background())
}
