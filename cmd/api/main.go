package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	_ "github.com/joho/godotenv/autoload"
	"github.com/rs/zerolog/log"
)

func main() {
	mainCtx, mainkillSwitch := context.WithCancel(context.Background())

	log.Info().Msg("environment initialized")

	db, err := createDatabaseConnection(mainCtx)
	if err != nil {
		log.Error().Err(err).Msg("couldn´t create database connection")
	}

	defer func() {
		if err = db.Connection.Disconnect(mainCtx); err != nil {
			panic(err)
		}
	}()

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		todoService(mainCtx, db.Connection, &wg)
	}()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	shutdownSequence := make(chan bool, 1)
	done := make(chan bool, 1)

	go func() {
		sig := <-sigs
		log.Debug().Msg(fmt.Sprintf("caught %s", sig.String()))
		shutdownSequence <- true
	}()
	<-shutdownSequence
	log.Info().Msg("attempting graceful shutdown")

	go func() {
		mainkillSwitch()
		wg.Wait()
		done <- true
	}()

	select {
	case <-time.After(20 * time.Second):
		log.Info().Msg("gracetime exceeded, hard stop now")
		break
	case <-done:
		log.Info().Msg("graceful shutdown successful, stopping now")
		break
	}
	log.Info().Msg("Bye!")
}
