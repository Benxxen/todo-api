module todo-api

go 1.16

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/rs/zerolog v1.21.0
	github.com/sethvargo/go-envconfig v0.3.5 // indirect
	go.mongodb.org/mongo-driver v1.5.1
)
